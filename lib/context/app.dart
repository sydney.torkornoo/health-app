import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';
import 'package:rydemi_health_care/model/patient.dart';

class MyAppState extends ChangeNotifier {
  var current = WordPair.random();
  var favourites = <WordPair>[];
  var showNav = true;
  var pageIndex = 0;
  var loggedIn = false;
  Patient? patient;
  void getNext() {
    current = WordPair.random();
    notifyListeners();
  }

  void setPatient(Patient? p) {
    patient = p;
  }

  void toggleFavourite() {
    if (favourites.contains(current)) {
      favourites.remove(current);
    } else {
      favourites.add(current);
    }
    notifyListeners();
  }

  void toggleShowNav() {
    showNav = !showNav;
    notifyListeners();
  }

  void updatePage(to) {
    pageIndex = to;
    // print('$pageIndex');
    notifyListeners();
  }

  void login() {
    loggedIn = true;
    notifyListeners();
  }
}
