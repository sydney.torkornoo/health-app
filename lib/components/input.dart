import 'package:flutter/material.dart';

class FormInput extends StatelessWidget {
  final String label;
  final Function f;
  final String? initialValue;
  final bool? obscureText;

  const FormInput(
      {super.key,
      required this.label,
      required this.f,
      this.initialValue,
      this.obscureText});
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 6),
          child: TextFormField(
            initialValue: initialValue ?? '',
            obscureText: obscureText ?? false,
            enableSuggestions: obscureText ?? true,
            autocorrect: obscureText ?? true,
            onChanged: (val) {
              f(val);
            },
            decoration: InputDecoration(
              border: UnderlineInputBorder(),
              labelText: label,
            ),
          ),
        ),
      ],
    );
  }
}
