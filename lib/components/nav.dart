import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../context/app.dart';

class MainNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appState = context.read<MyAppState>();
    List<NavigationRailDestination> destinations = [
      NavigationRailDestination(
        icon: Icon(Icons.dashboard),
        label: Text('Dashboard'),
      ),
      NavigationRailDestination(
        icon: Icon(Icons.medication_liquid),
        label: Text('Patients'),
      ),
      // NavigationRailDestination(
      //   icon: Icon(Icons.calendar_month),
      //   label: Text('Add Patient'),
      // ),
      NavigationRailDestination(
        icon: Icon(Icons.medical_services),
        label: Text('Doctors'),
      ),
      NavigationRailDestination(
        icon: Icon(Icons.calendar_month),
        label: Text('Appointments'),
      ),
    ];
    return NavigationRail(
      extended: appState.showNav,
      leading: ElevatedButton(
        // elevation: 0,
        onPressed: appState.toggleShowNav,
        child: const Icon(Icons.view_array),
      ),
      groupAlignment: 0,
      destinations: destinations,
      selectedIndex:
          appState.pageIndex > destinations.length - 1 ? 0 : appState.pageIndex,
      onDestinationSelected: (value) {
        appState.updatePage(value);
      },
    );
  }
}
