class Template {
  final int id;
  const Template({required this.id});
  factory Template.fromJson(Map<String, dynamic> json) {
    return switch (json) {
      _ => throw const FormatException('Failed to load patient.'),
    };
  }
}
