class Patient {
  String id;
  String name;
  String phoneNumber;
  String? dob;
  String gender;
  String status;
  DateTime registeredOn;
  int version;

  Patient({
    required this.id,
    required this.name,
    required this.phoneNumber,
    this.dob,
    required this.gender,
    required this.status,
    required this.registeredOn,
    required this.version,
  });

  // Factory constructor to create a Patient from JSON
  factory Patient.fromJson(Map<String, dynamic> json) {
    return Patient(
      id: json['id'],
      name: json['name'],
      phoneNumber: json['phoneNumber'],
      dob: json['dob'],
      gender: json['gender'],
      status: json['status'],
      registeredOn: DateTime.fromMillisecondsSinceEpoch(json['registeredOn']),
      version: json['__v'],
    );
  }

  // Method to convert a Patient to JSON
  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'name': name,
      'phoneNumber': phoneNumber,
      'dob': dob,
      'gender': gender,
      'status': status,
      'registeredOn': registeredOn.millisecondsSinceEpoch,
      '__v': version,
    };
  }
}
