import 'package:rydemi_health_care/model/doctor.dart';
import 'package:rydemi_health_care/model/patient.dart';

class Log {
  Doctor? doctorId;
  Patient? patientId;
  String date;
  String time;
  String description;
  String completed;
  String billable;
  String amount;
  String paid;

  Log({
    this.doctorId,
    this.patientId,
    required this.date,
    required this.time,
    required this.description,
    required this.completed,
    required this.billable,
    required this.amount,
    required this.paid,
  });

  // Factory constructor to create a Log from JSON
  factory Log.fromJson(Map<String, dynamic> json) {
    return Log(
      doctorId: json['doctorId'],
      patientId: json['patientId'],
      date: json['date'],
      time: json['time'],
      description: json['description'],
      completed: json['completed'],
      billable: json['billable'],
      amount: json['amount'],
      paid: json['paid'],
    );
  }

  // Method to convert a Log to JSON
  Map<String, dynamic> toJson() {
    return {
      'doctorId': doctorId,
      'patientId': patientId,
      'date': date,
      'time': time,
      'description': description,
      'completed': completed,
      'billable': billable,
      'amount': amount,
      'paid': paid,
    };
  }
}
