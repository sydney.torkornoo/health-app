import 'package:rydemi_health_care/model/schedule.dart';

class Profile {
  List<Schedule> appointments;

  Profile({
    required this.appointments,
  });

  // Factory constructor to create a Profile from JSON
  factory Profile.fromJson(Map<String, dynamic> json) {
    return Profile(
      appointments: json['appointments'],
    );
  }

  // Method to convert a Profile to JSON
  Map<String, dynamic> toJson() {
    return {
      'appointments': appointments,
    };
  }
}
