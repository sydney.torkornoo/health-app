import 'package:rydemi_health_care/model/patient.dart';
import './doctor.dart';

class Schedule {
  String? id;
  String? doctor;
  String? patient;
  String? date;
  String? time;
  String? duration;
  DateTime? registeredOn;
  Doctor? doctorId;
  Patient? patientId;

  Schedule({
    this.id,
    this.doctor,
    this.patient,
    this.time,
    this.duration,
    this.date,
    this.registeredOn,
    this.patientId,
    this.doctorId,
  });

  // Factory constructor to create a Schedule from JSON
  factory Schedule.fromJson(Map<String, dynamic> json) {
    return Schedule(
      id: json['id'],
      doctor: json['doctor'],
      patient: json['patient'],
      time: json['time'],
      date: json['date'],
      duration: json['duration'],
      patientId: json['patientId'],
      doctorId: json['doctorId'],
      registeredOn: DateTime.fromMillisecondsSinceEpoch(json['registeredOn']),
    );
  }

  // Method to convert a Schedule to JSON
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'doctor': doctor,
      'patient': patient,
      'duration': duration,
      'date': date,
      'doctorId': doctorId,
      'patientId': patientId,
      'registeredOn': registeredOn,
    };
  }
}
