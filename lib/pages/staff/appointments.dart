import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
import 'package:rydemi_health_care/components/card.dart';
import 'package:rydemi_health_care/components/heading.dart';
import 'package:rydemi_health_care/components/sub_heading.dart';
import 'package:rydemi_health_care/helpers/util.dart';
import 'package:rydemi_health_care/model/patient.dart';
import 'package:rydemi_health_care/model/doctor.dart';
import 'package:rydemi_health_care/model/schedule.dart';
// import '../../context/app.dart';

class Appointments extends StatefulWidget {
  @override
  State<Appointments> createState() => _AppointmentsState();
}

class _AppointmentsState extends State<Appointments> {
  List<Schedule> appointments = [];
  bool loaded = false;
  fetch() async {
    if (loaded) {
      return true;
    }
    final response = await http.get(
      Uri.parse('${Utils.url}/appointment'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    // print(response.body);
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      Map<String, dynamic> jsonData = json.decode(response.body);
      List<Schedule> d = [];
      jsonData.forEach((key, value) {
        if (value is List) {
          // print(value);
          for (var element in value) {
            Patient p = Patient.fromJson(element['patientId']);
            Doctor dr = Doctor.fromJson(element['doctorId']);
            d.add(
              Schedule.fromJson({...element, 'doctorId': dr, 'patientId': p}),
            );
          }
        }
      });
      // print(d);
      setState(() {
        appointments = d;
        loaded = true;
      });
    } else {
      throw Exception('Failed to load patients.');
    }
  }

  @override
  Widget build(BuildContext context) {
    // var appState = context.read()<MyAppState>();
    fetch();
    List<Widget> widgets = [];

    widgets.add(HeadingText(text: 'Doctor Patient Appointments'));
    widgets.add(
      Row(
        children: [
          Expanded(
            // flex: 2,
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'All Appointments'),
                    HeadingText(text: '6'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'Free Slots (Today)'),
                    SubHeadingText(text: '3'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    ElevatedButton(
                        onPressed: () {
                          // appState.updatePage(5);
                        },
                        child: Text('Add Patient')),
                    SizedBox(
                      width: 5,
                    ),
                    ElevatedButton(
                        onPressed: () {
                          // appState.updatePage(1);
                        },
                        child: Text('View')),
                  ],
                ),
              ],
            )),
          ),
          Expanded(
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'Quick Actions'),
                    // HeadingText(text: '28'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'Online'),
                    SubHeadingText(text: '5'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    ElevatedButton(
                        onPressed: () {
                          // appState.updatePage(6);
                        },
                        child: Text('Schedule Appointment')),
                  ],
                ),
              ],
            )),
          ),
        ],
      ),
    );
    for (var appointment in appointments) {
      // print('looping');
      widgets.add(
        SizedBox(
          child: Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CircleAvatar(
                            radius: 50,
                            backgroundImage: AssetImage('images/bg.png'),
                          ),
                        ),
                        Text(appointment.doctorId?.name ?? '')
                      ],
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(Icons.arrow_forward),
                    Text('Has an appointment with'),
                    Icon(Icons.arrow_forward),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CircleAvatar(
                            radius: 50,
                            backgroundImage: AssetImage('images/bg.png'),
                          ),
                        ),
                        Text(appointment.patientId?.name ?? '')
                      ],
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Text(
                    'On ${appointment.date ?? ''} at ${appointment.time ?? ''} for ${appointment.duration ?? ''}',
                    textAlign: TextAlign.center,
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.start,
                  children: [
                    ElevatedButton(
                      // textColor: const Color(0xFF6200EE),
                      onPressed: () {
                        // Perform some action
                      },
                      child: const Text('Reschedule'),
                    ),
                    ElevatedButton(
                      // textColor: const Color(0xFF6200EE),
                      onPressed: () {
                        // Perform some action
                      },
                      child: const Text('Cancel'),
                    ),
                  ],
                ),
                // Image.asset('assets/card-sample-image.jpg'),
                // Image.asset('assets/card-sample-image-2.jpg'),
              ],
            ),
          ),
        ),
      );
    }

    return ListView(
      padding: const EdgeInsets.all(8),
      children: widgets,
    );
  }
}
