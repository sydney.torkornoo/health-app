import 'dart:convert';
// import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rydemi_health_care/components/card.dart';
import 'package:rydemi_health_care/components/heading.dart';
import 'package:rydemi_health_care/components/input.dart';
import 'package:rydemi_health_care/components/sub_heading.dart';
// import 'package:rydemi_health_care/model/patient.dart';
import 'package:http/http.dart' as http;
import 'package:rydemi_health_care/helpers/util.dart';
import '../../context/app.dart';

import 'package:toastification/toastification.dart';
// import 'package:provider/provider.dart';
// import 'package:rydemi_health_care/components/heading.dart';
// import 'package:rydemi_health_care/components/input.dart';
// import 'package:rydemi_health_care/components/sub_heading.dart';
// import '../../components/card.dart';
// import '../../context/app.dart';

class AddLog extends StatefulWidget {
  @override
  State<AddLog> createState() => _AddLogState();
}

class _AddLogState extends State<AddLog> {
  // final List<String> entries = <String>['A', 'B', 'C'];
  var name = '';
  var dob = '';
  var gender = '';
  var id = '';
  var phoneNumber = '';
  var redirect = false;

  var doctorId = '';
  var patientId = '';
  var date = '';
  var time = '';
  var description = '';
  var completed = '';
  var billable = '';
  var amount = '';
  var paid = '';

  Future<bool> create() async {
    print({
      'doctorId': doctorId,
      'patientId': patientId,
      'date': date,
      'time': time,
      'description': description,
      'completed': completed,
      'billable': billable,
      'amount': amount,
      'paid': paid,
    });
    // return false;
    // return Patient(id: 'id', name: 'name', dob: 'dob', gender: 'gender');
    final response = await http.post(
      // Uri.parse('http://localhost:3113/log'),
      Uri.parse('${Utils.url}/log'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'doctorId': doctorId,
        'patientId': patientId,
        'date': date,
        'time': time,
        'description': description,
        'completed': completed,
        'billable': billable,
        'amount': amount,
        'paid': paid,
      }),
    );

    if (response.statusCode == 201) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      toastification.show(
        // context: context, // optional if you use ToastificationWrapper
        title: Text('Added Log Successfully.'),
        autoCloseDuration: const Duration(seconds: 5),
        type: ToastificationType.info,
        style: ToastificationStyle.flat,
      );
      return true;
      // return Patient.fromJson(
      //     jsonDecode(response.body) as Map<String, dynamic>);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to create patient.');
    }
  }

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    setState(() {
      patientId = appState.patient!.id;
    });
    List<Widget> widgets = [];
    widgets.add(HeadingText(text: 'Patient Logs'));
    widgets.add(
      Row(
        children: [
          Expanded(
            // flex: 2,
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'Log Entries'),
                    HeadingText(text: '435'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'Some Info'),
                    SubHeadingText(text: '99'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    ElevatedButton(
                        onPressed: () {}, child: Text('Add Patient')),
                    SizedBox(
                      width: 5,
                    ),
                    ElevatedButton(onPressed: () {}, child: Text('View')),
                  ],
                ),
              ],
            )),
          ),
          Expanded(
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'Quick Actions'),
                    // HeadingText(text: '28'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'Online'),
                    SubHeadingText(text: '5'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    ElevatedButton(onPressed: () {}, child: Text('Add Doctor')),
                  ],
                ),
              ],
            )),
          ),
        ],
      ),
    );
    widgets.add(
      Card(
        clipBehavior: Clip.antiAlias,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('images/bg.png'),
              ),
            ),
            HeadingText(text: 'Patient Log'),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                FormInput(
                  label: "Overseeing Doctor (Doctor's ID)",
                  f: (val) {
                    setState(() {
                      doctorId = val;
                    });
                  },
                ),
                FormInput(
                  label: "Date",
                  f: (val) {
                    setState(() {
                      date = val;
                    });
                  },
                ),
                FormInput(
                  label: "Time",
                  f: (val) {
                    setState(() {
                      time = val;
                    });
                  },
                ),
                FormInput(
                  label: "Description",
                  f: (val) {
                    setState(() {
                      description = val;
                    });
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Billable? ${billable.toUpperCase()}'),
                      Row(
                        children: [
                          ElevatedButton(
                            // textColor: const Color(0xFF6200EE),
                            onPressed: () {
                              // Perform some action
                              setState(() {
                                billable = 'yes';
                              });
                            },
                            child: const Text('Yes'),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          ElevatedButton(
                            // textColor: const Color(0xFF6200EE),
                            onPressed: () async {
                              // Perform some action
                              setState(() {
                                billable = 'no';
                              });
                            },
                            child: const Text('No'),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                billable == 'yes'
                    ? FormInput(
                        label: "Amount",
                        f: (val) {
                          setState(() {
                            amount = val;
                          });
                        },
                      )
                    : SizedBox(),
                billable == 'yes'
                    ? Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Patient Already Paid? ${paid.toUpperCase()}'),
                            Row(
                              children: [
                                ElevatedButton(
                                  // textColor: const Color(0xFF6200EE),
                                  onPressed: () {
                                    // Perform some action
                                    setState(() {
                                      paid = 'yes';
                                    });
                                  },
                                  child: const Text('Yes'),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                ElevatedButton(
                                  // textColor: const Color(0xFF6200EE),
                                  onPressed: () async {
                                    // Perform some action
                                    setState(() {
                                      paid = 'no';
                                    });
                                  },
                                  child: const Text('No'),
                                ),
                              ],
                            )
                          ],
                        ),
                      )
                    : SizedBox(),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                          'An already completed Procedure? ${completed.toUpperCase()}'),
                      Row(
                        children: [
                          ElevatedButton(
                            // textColor: const Color(0xFF6200EE),
                            onPressed: () {
                              // Perform some action
                              setState(() {
                                completed = 'yes';
                              });
                            },
                            child: const Text('Yes'),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          ElevatedButton(
                            // textColor: const Color(0xFF6200EE),
                            onPressed: () async {
                              // Perform some action
                              setState(() {
                                completed = 'no';
                              });
                            },
                            child: const Text('No'),
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.start,
              children: [
                ElevatedButton(
                  // textColor: const Color(0xFF6200EE),
                  onPressed: () async {
                    // Perform some action
                    await create();
                    appState.updatePage(7);
                  },
                  child: const Text('Create Patient Record'),
                ),
              ],
            ),
            // Image.asset('assets/card-sample-image.jpg'),
            // Image.asset('assets/card-sample-image-2.jpg'),
          ],
        ),
      ),
    );

    return ListView(
      padding: const EdgeInsets.all(8),
      children: widgets,
    );
  }
}
