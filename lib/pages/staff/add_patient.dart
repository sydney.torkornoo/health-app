import 'dart:convert';
// import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rydemi_health_care/components/card.dart';
import 'package:rydemi_health_care/components/heading.dart';
import 'package:rydemi_health_care/components/input.dart';
import 'package:rydemi_health_care/components/sub_heading.dart';
// import 'package:rydemi_health_care/model/patient.dart';
import 'package:http/http.dart' as http;
import 'package:rydemi_health_care/helpers/util.dart';
import 'package:toastification/toastification.dart';
import '../../context/app.dart';

// import 'package:provider/provider.dart';
// import 'package:rydemi_health_care/components/heading.dart';
// import 'package:rydemi_health_care/components/input.dart';
// import 'package:rydemi_health_care/components/sub_heading.dart';
// import '../../components/card.dart';
// import '../../context/app.dart';

class AddPatient extends StatefulWidget {
  @override
  State<AddPatient> createState() => _AddPatientState();
}

class _AddPatientState extends State<AddPatient> {
  // final List<String> entries = <String>['A', 'B', 'C'];
  var name = '';
  var dob = '';
  var gender = '';
  var id = '';
  var phoneNumber = '';
  var redirect = false;

  Future<bool> create() async {
    print({
      'name': name,
      'dob': dob,
      'gender': gender,
      'id': id,
      'phoneNumber': phoneNumber,
    });
    // return Patient(id: 'id', name: 'name', dob: 'dob', gender: 'gender');
    final response = await http.post(
      Uri.parse('${Utils.url}/patient'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'name': name,
        'dob': dob,
        'gender': gender,
        'id': id,
        'phoneNumber': phoneNumber,
      }),
    );

    if (response.statusCode == 201) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      return true;
      // return Patient.fromJson(
      //     jsonDecode(response.body) as Map<String, dynamic>);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to create patient.');
    }
  }

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();

    List<Widget> widgets = [];
    widgets.add(HeadingText(text: 'Registered Patients'));
    widgets.add(
      Row(
        children: [
          Expanded(
            // flex: 2,
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'All Patients'),
                    HeadingText(text: '330'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'Checked-in Patients'),
                    SubHeadingText(text: '25'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    ElevatedButton(
                        onPressed: () {}, child: Text('Add Patient')),
                    SizedBox(
                      width: 5,
                    ),
                    ElevatedButton(
                        onPressed: () {
                          appState.updatePage(1);
                        },
                        child: Text('View')),
                  ],
                ),
              ],
            )),
          ),
          Expanded(
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'Quick Actions'),
                    // HeadingText(text: '28'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'Online'),
                    SubHeadingText(text: '5'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    ElevatedButton(
                        onPressed: () {
                          appState.updatePage(4);
                        },
                        child: Text('Add Doctor')),
                  ],
                ),
              ],
            )),
          ),
        ],
      ),
    );
    widgets.add(
      Card(
        clipBehavior: Clip.antiAlias,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('images/bg.png'),
              ),
            ),
            HeadingText(text: 'Patient Details'),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                FormInput(
                  label: "Patient's full name",
                  f: (val) {
                    setState(() {
                      name = val;
                    });
                  },
                ),
                FormInput(
                  label: "Date of Birth",
                  f: (val) {
                    setState(() {
                      dob = val;
                    });
                  },
                ),
                FormInput(
                  label: "National ID Number",
                  f: (val) {
                    setState(() {
                      id = val;
                    });
                  },
                ),
                FormInput(
                  label: "Gender",
                  f: (val) {
                    setState(() {
                      gender = val;
                    });
                  },
                ),
                FormInput(
                  label: "Phone Number",
                  f: (val) {
                    setState(() {
                      phoneNumber = val;
                    });
                  },
                ),
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.start,
              children: [
                ElevatedButton(
                  // textColor: const Color(0xFF6200EE),
                  onPressed: () async {
                    // Perform some action

                    final added = await create();

                    if (added) {
                      toastification.show(
                        // optional if you use ToastificationWrapper
                        title: Text('New Patient $name added successfully'),
                        autoCloseDuration: const Duration(seconds: 5),
                        type: ToastificationType.info,
                        style: ToastificationStyle.flat,
                      );
                      appState.updatePage(1);
                    }
                  },
                  child: const Text('Create Patient Record'),
                ),
              ],
            ),
            // Image.asset('assets/card-sample-image.jpg'),
            // Image.asset('assets/card-sample-image-2.jpg'),
          ],
        ),
      ),
    );

    return ListView(
      padding: const EdgeInsets.all(8),
      children: widgets,
    );
  }
}
