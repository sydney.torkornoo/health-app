import 'dart:convert';
// import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rydemi_health_care/components/card.dart';
import 'package:rydemi_health_care/components/heading.dart';
import 'package:rydemi_health_care/components/input.dart';
import 'package:rydemi_health_care/components/sub_heading.dart';
// import 'package:rydemi_health_care/model/patient.dart';
import 'package:http/http.dart' as http;
import 'package:rydemi_health_care/helpers/util.dart';
import 'package:toastification/toastification.dart';
import '../../context/app.dart';

// import 'package:provider/provider.dart';
// import 'package:rydemi_health_care/components/heading.dart';
// import 'package:rydemi_health_care/components/input.dart';
// import 'package:rydemi_health_care/components/sub_heading.dart';
// import '../../components/card.dart';
// import '../../context/app.dart';

class ScheduleAppointment extends StatefulWidget {
  @override
  State<ScheduleAppointment> createState() => _ScheduleAppointmentState();
}

class _ScheduleAppointmentState extends State<ScheduleAppointment> {
  // final List<String> entries = <String>['A', 'B', 'C'];
  String doctorId = '';
  String patientId = '';
  String date = '';
  String time = '';
  String duration = '';

  Future<bool> create() async {
    // return Patient(id: 'id', name: 'name', dob: 'dob', gender: 'gender');
    final response = await http.post(
      Uri.parse('${Utils.url}/appointment'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'doctorId': doctorId,
        'patientId': patientId,
        'date': date,
        'time': time,
        'duration': duration,
      }),
    );

    if (response.statusCode == 201) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      return true;
      // return Patient.fromJson(
      //     jsonDecode(response.body) as Map<String, dynamic>);
    } else {
      // print(response);
      return false;
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      // throw Exception('Failed to schedlue appointment.');
    }
  }

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();

    if (appState.patient != null) {
      setState(() {
        patientId = appState.patient!.id;
        print(appState.patient!.id);
        appState.setPatient(null);
      });
    }

    List<Widget> widgets = [];
    widgets.add(HeadingText(text: 'Upcoming Appointments'));
    widgets.add(
      Row(
        children: [
          Expanded(
            // flex: 2,
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'Appointments'),
                    HeadingText(text: '34'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'Free Slots'),
                    SubHeadingText(text: '15'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    // ElevatedButton(
                    //     onPressed: () {}, child: Text('Add Patient')),
                    // SizedBox(
                    //   width: 5,
                    // ),
                    ElevatedButton(onPressed: () {}, child: Text('View')),
                  ],
                ),
              ],
            )),
          ),
          Expanded(
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'Quick Actions'),
                    // HeadingText(text: '28'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'Due Today'),
                    SubHeadingText(text: '5'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    ElevatedButton(
                        onPressed: () {}, child: Text('Add Billing')),
                  ],
                ),
              ],
            )),
          ),
        ],
      ),
    );
    widgets.add(
      Card(
        clipBehavior: Clip.antiAlias,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('images/bg.png'),
              ),
            ),
            HeadingText(text: 'Create new schedule'),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                FormInput(
                  initialValue: patientId,
                  label: "Patient's ID",
                  f: (val) {
                    setState(() {
                      patientId = val;
                    });
                  },
                ),
                FormInput(
                  label: "Doctor's ID",
                  f: (val) {
                    setState(() {
                      doctorId = val;
                    });
                  },
                ),
                FormInput(
                  label: "Appointment Date",
                  f: (val) {
                    setState(() {
                      date = val;
                    });
                  },
                ),
                FormInput(
                  label: "Appointment Time",
                  f: (val) {
                    setState(() {
                      time = val;
                    });
                  },
                ),
                FormInput(
                  label: "Duration",
                  f: (val) {
                    setState(() {
                      duration = val;
                    });
                  },
                ),
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.start,
              children: [
                ElevatedButton(
                  // textColor: const Color(0xFF6200EE),
                  onPressed: () async {
                    // Perform some action
                    final added = await create();

                    if (added) {
                      toastification.show(
                        // optional if you use ToastificationWrapper
                        title: Text('New Appointment scheduled successfully'),
                        autoCloseDuration: const Duration(seconds: 5),
                        type: ToastificationType.info,
                        style: ToastificationStyle.flat,
                      );
                      appState.updatePage(3);
                    }
                  },
                  child: const Text('Schedule Appointment'),
                ),
              ],
            ),
            // Image.asset('assets/card-sample-image.jpg'),
            // Image.asset('assets/card-sample-image-2.jpg'),
          ],
        ),
      ),
    );

    return ListView(
      padding: const EdgeInsets.all(8),
      children: widgets,
    );
  }
}
