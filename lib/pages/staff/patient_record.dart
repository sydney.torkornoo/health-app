import 'dart:convert';
// import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:rydemi_health_care/components/card.dart';
import 'package:rydemi_health_care/components/heading.dart';
import 'package:rydemi_health_care/components/input.dart';
import 'package:rydemi_health_care/components/sub_heading.dart';
// import 'package:rydemi_health_care/model/patient.dart';
import 'package:http/http.dart' as http;
import 'package:rydemi_health_care/helpers/util.dart';
import 'package:rydemi_health_care/model/doctor.dart';
import 'package:rydemi_health_care/model/log.dart';
import 'package:rydemi_health_care/model/patient.dart';
import 'package:rydemi_health_care/model/schedule.dart';
import '../../context/app.dart';

// import 'package:provider/provider.dart';
// import 'package:rydemi_health_care/components/heading.dart';
// import 'package:rydemi_health_care/components/input.dart';
// import 'package:rydemi_health_care/components/sub_heading.dart';
// import '../../components/card.dart';
// import '../../context/app.dart';

class PatientRecord extends StatefulWidget {
  @override
  State<PatientRecord> createState() => _PatientRecordState();
}

class _PatientRecordState extends State<PatientRecord> {
  // final List<String> entries = <String>['A', 'B', 'C'];
  var name = '';
  var dob = '';
  var gender = '';
  var id = '';
  var phoneNumber = '';
  var redirect = false;

  bool loaded = false;
  List<Schedule> appointments = [];
  List<Log> logs = [];
  Future<bool> create() async {
    print({
      'name': name,
      'dob': dob,
      'gender': gender,
      'id': id,
      'phoneNumber': phoneNumber,
    });
    // return Patient(id: 'id', name: 'name', dob: 'dob', gender: 'gender');
    final response = await http.post(
      Uri.parse('${Utils.url}/patient'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'name': name,
        'dob': dob,
        'gender': gender,
        'id': id,
        'phoneNumber': phoneNumber,
      }),
    );

    if (response.statusCode == 201) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      return true;
      // return Patient.fromJson(
      //     jsonDecode(response.body) as Map<String, dynamic>);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to create album.');
    }
  }

  fetchProfile(String id) async {
    if (loaded) {
      return true;
    }
    final response = await http.get(
      Uri.parse('${Utils.url}/patient/$id'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      Map<String, dynamic> jsonData = json.decode(response.body);
      // print(response.body);
      // List<Patient> p = [];
      jsonData.forEach((key, value) {
        if (value is List) {
          for (var element in value) {
            print(key);

            if (key == 'appointments') {
              element.remove('patientId');
              // element.remove('doctorId');
              print(element);
              element['doctorId'] = Doctor.fromJson(element['doctorId']);
              appointments.add(Schedule.fromJson(element));
            } else if (key == 'logs') {
              element.remove('patientId');
              // element.remove('doctorId');
              // print(element);
              element['doctorId'] = Doctor.fromJson(element['doctorId']);
              element['paid'] = element['paid'] ? 'yes' : 'no';
              element['billable'] = element['billable'] ? 'yes' : 'no';
              element['completed'] = element['completed'] ? 'yes' : 'no';
              element['amount'] =
                  element['amount'].toString(); //? 'yes' : 'no';
              logs.add(Log.fromJson(element));
            }
          }
        }
      });
      setState(() {
        loaded = true;
      });
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      print(response);
      throw Exception('Failed to load patients.');
    }
  }

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    Patient? patient = appState.patient;
    if (!loaded) {
      fetchProfile(patient!.id);
    }
    List<Widget> widgets = [];
    List<Widget> scheduleWidgets = [];
    List<Widget> logsWidgets = [];
    widgets.add(HeadingText(text: 'Patient Record'));
    widgets.add(SubHeadingText(
        text:
            'Currently Checked ${patient!.status == 'checked-in' ? "In" : "Out"}'));
    widgets.add(
      Row(
        children: [
          Expanded(
            // flex: 2,
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'All Patients'),
                    HeadingText(text: '330'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'Checked-in Patients'),
                    SubHeadingText(text: '25'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    ElevatedButton(
                        onPressed: () {}, child: Text('Add Patient')),
                    SizedBox(
                      width: 5,
                    ),
                    ElevatedButton(onPressed: () {}, child: Text('View')),
                  ],
                ),
              ],
            )),
          ),
          Expanded(
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'Quick Actions'),
                    // HeadingText(text: '28'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'Online'),
                    SubHeadingText(text: '5'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    ElevatedButton(onPressed: () {}, child: Text('Add Doctor')),
                  ],
                ),
              ],
            )),
          ),
        ],
      ),
    );
    widgets.add(
      Card(
        clipBehavior: Clip.antiAlias,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('images/bg.png'),
              ),
            ),
            HeadingText(text: 'Patient Details'),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                FormInput(
                  initialValue: patient.name,
                  label: "Patient's full name",
                  f: (val) {
                    setState(() {
                      name = val;
                    });
                  },
                ),
                FormInput(
                  initialValue: patient.dob,
                  label: "Date of Birth",
                  f: (val) {
                    setState(() {
                      dob = val;
                    });
                  },
                ),
                FormInput(
                  initialValue: patient.id,
                  label: "National ID Number",
                  f: (val) {
                    setState(() {
                      id = val;
                    });
                  },
                ),
                FormInput(
                  initialValue: patient.gender,
                  label: "Gender",
                  f: (val) {
                    setState(() {
                      gender = val;
                    });
                  },
                ),
                FormInput(
                  initialValue: patient.phoneNumber,
                  label: "Phone Number",
                  f: (val) {
                    setState(() {
                      phoneNumber = val;
                    });
                  },
                ),
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.start,
              children: [
                ElevatedButton(
                  // textColor: const Color(0xFF6200EE),
                  onPressed: () async {
                    // Perform some action
                    await create();
                    appState.updatePage(1);
                  },
                  child: const Text('Create Patient Record'),
                ),
              ],
            ),
            // Image.asset('assets/card-sample-image.jpg'),
            // Image.asset('assets/card-sample-image-2.jpg'),
          ],
        ),
      ),
    );
    widgets.add(SizedBox(
      height: 20,
    ));
    widgets.add(SubHeadingText(text: 'History Log'));
    widgets.add(HeadingText(text: 'Appointments'));

    for (var a in appointments) {
      scheduleWidgets.add(
        Card(
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: SubHeadingText(
                    text: 'Appointment with ${a.doctorId?.name}'),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10.0, vertical: 2),
                child: Text(
                  'On ${a.date} at ${a.time}',
                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10.0, vertical: 2),
                child: Text(
                  'For ${a.duration}',
                  style: TextStyle(
                    color: Colors.black.withOpacity(0.6),
                  ),
                ),
              ),
              // ButtonBar(
              //   alignment: MainAxisAlignment.start,
              //   children: [
              //     ElevatedButton(
              //       // textColor: const Color(0xFF6200EE),
              //       onPressed: () {
              //         // Perform some action
              //       },
              //       child: const Text('ACTION 1'),
              //     ),
              //     ElevatedButton(
              //       // textColor: const Color(0xFF6200EE),
              //       onPressed: () {
              //         // Perform some action
              //       },
              //       child: const Text('ACTION 2'),
              //     ),
              //   ],
              // ),
              // // Image.asset('assets/card-sample-image.jpg'),
              // Image.asset('assets/card-sample-image-2.jpg'),
            ],
          ),
        ),
      );
    }

    for (var a in logs) {
      logsWidgets.add(
        Card(
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: [
              ListTile(
                // leading: Icon(Icons.arrow_drop_down_circle),
                title: Text(a.description),
                trailing: Text(
                  'By ${a.doctorId?.name}',
                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                ),
                subtitle: Text(
                  textAlign: TextAlign.left,
                  'On ${a.date}, ${a.billable == 'yes' && a.paid == 'no' ? 'Pending payment, \$${a.amount != 'null' ? a.amount : '0.00'}' : 'Payment Complete'}, ${a.completed == 'yes' ? 'Procedure Completed' : 'Procedure pending completion'}',
                  style: TextStyle(
                    color: Colors.black.withOpacity(0.6),
                  ),
                ),
              ),
              // Padding(
              //   padding: const EdgeInsets.all(10.0),
              //   child: Text(
              //     textAlign: TextAlign.left,
              //     'Greyhound divisively hello coldly wonderfully marginally far upon excluding.',
              //     style: TextStyle(
              //       color: Colors.black.withOpacity(0.6),
              //     ),
              //   ),
              // ),
              // // ButtonBar(
              //   alignment: MainAxisAlignment.start,
              //   children: [
              //     ElevatedButton(
              //       // textColor: const Color(0xFF6200EE),
              //       onPressed: () {
              //         // Perform some action
              //       },
              //       child: const Text('ACTION 1'),
              //     ),
              //     ElevatedButton(
              //       // textColor: const Color(0xFF6200EE),
              //       onPressed: () {
              //         // Perform some action
              //       },
              //       child: const Text('ACTION 2'),
              //     ),
              //   ],
              // ),
              // // Image.asset('assets/card-sample-image.jpg'),
              // Image.asset('assets/card-sample-image-2.jpg'),
            ],
          ),
        ),
      );
    }
    widgets.add(Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      height: 160,
      child: ListView(
          // This next line does the trick.
          scrollDirection: Axis.horizontal,
          children: scheduleWidgets),
    ));
    widgets.add(HeadingText(text: 'Patient\'s History'));
    // widgets.add(logsWidgets)
    widgets.add(
      Column(
        // This next line does the trick.
        // scrollDirection: Axis.vertical,
        children: logsWidgets,
      ),
    );
    return ListView(
      padding: const EdgeInsets.all(18),
      children: widgets,
    );
  }
}
