import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rydemi_health_care/components/card.dart';
import 'package:rydemi_health_care/components/heading.dart';
import 'package:rydemi_health_care/components/input.dart';
import 'package:rydemi_health_care/components/sub_heading.dart';
import 'package:rydemi_health_care/helpers/util.dart';
// import 'package:rydemi_health_care/model/doctor.dart';
import 'package:http/http.dart' as http;
import 'package:toastification/toastification.dart';

// import 'package:provider/provider.dart';
// import 'package:rydemi_health_care/components/heading.dart';
// import 'package:rydemi_health_care/components/input.dart';
// import 'package:rydemi_health_care/components/sub_heading.dart';
// import '../../components/card.dart';
import '../../context/app.dart';

class AddDoctor extends StatefulWidget {
  @override
  State<AddDoctor> createState() => _AddDoctorState();
}

class _AddDoctorState extends State<AddDoctor> {
  // final List<String> entries = <String>['A', 'B', 'C'];
  var name = '';
  var dob = '';
  var gender = '';
  var id = '';
  var phoneNumber = '';

  create() async {
    print({
      'name': name,
      'dob': dob,
      'gender': gender,
      'id': id,
      'phoneNumber': phoneNumber,
    });
    // return Doctor(id: 'id', name: 'name', dob: 'dob', gender: 'gender');
    final response = await http.post(
      Uri.parse('${Utils.url}/doctor'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'name': name,
        'dob': dob,
        'gender': gender,
        'id': id,
        'phoneNumber': phoneNumber,
      }),
    );

    if (response.statusCode == 201) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      return true;
      // Doctor.fromJson(jsonDecode(response.body) as Map<String, dynamic>);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      return false;
      // throw Exception('Failed to create album.');
    }
  }

  @override
  Widget build(BuildContext context) {
    var appState = context.read<MyAppState>();
    List<Widget> widgets = [];
    widgets.add(HeadingText(text: 'Hospital Doctors'));
    widgets.add(
      Row(
        children: [
          Expanded(
            // flex: 2,
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'All Doctors'),
                    HeadingText(text: '28'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'On Duty'),
                    SubHeadingText(text: '15'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    ElevatedButton(onPressed: () {}, child: Text('Extra Info')),
                    SizedBox(
                      width: 5,
                    ),
                    ElevatedButton(onPressed: () {}, child: Text('View')),
                  ],
                ),
              ],
            )),
          ),
          Expanded(
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'Quick Actions'),
                    // HeadingText(text: '28'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'Online'),
                    SubHeadingText(text: '5'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    ElevatedButton(
                        onPressed: () {
                          appState.updatePage(4);
                        },
                        child: Text('Add Doctor')),
                  ],
                ),
              ],
            )),
          ),
        ],
      ),
    );
    widgets.add(
      Card(
        clipBehavior: Clip.antiAlias,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('images/bg.png'),
              ),
            ),
            HeadingText(text: 'Add New Doctor'),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                FormInput(
                  label: "Doctor's full name",
                  f: (val) {
                    setState(() {
                      name = val;
                    });
                  },
                ),
                FormInput(
                  label: "Date of Birth",
                  f: (val) {
                    setState(() {
                      dob = val;
                    });
                  },
                ),
                FormInput(
                  label: "National ID Number",
                  f: (val) {
                    setState(() {
                      id = val;
                    });
                  },
                ),
                FormInput(
                  label: "Gender",
                  f: (val) {
                    setState(() {
                      gender = val;
                    });
                  },
                ),
                FormInput(
                  label: "Phone Number",
                  f: (val) {
                    setState(() {
                      phoneNumber = val;
                    });
                  },
                ),
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.start,
              children: [
                ElevatedButton(
                  // textColor: const Color(0xFF6200EE),
                  onPressed: () async {
                    // Perform some action
                    final added = await create();
                    if (added) {
                      toastification.show(
                        // optional if you use ToastificationWrapper
                        title: Text('New Doctor $name added successfully'),
                        autoCloseDuration: const Duration(seconds: 5),
                        type: ToastificationType.info,
                        style: ToastificationStyle.flat,
                      );
                      appState.updatePage(2);
                    }
                  },
                  child: const Text('Create Doctor Record'),
                ),
              ],
            ),
            // Image.asset('assets/card-sample-image.jpg'),
            // Image.asset('assets/card-sample-image-2.jpg'),
          ],
        ),
      ),
    );
    return ListView(
      padding: const EdgeInsets.all(8),
      children: widgets,
    );
  }
}
