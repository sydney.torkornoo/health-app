import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:rydemi_health_care/components/heading.dart';
// import 'package:rydemi_health_care/components/input.dart';
import 'package:rydemi_health_care/components/sub_heading.dart';
import 'package:rydemi_health_care/helpers/util.dart';
import 'package:rydemi_health_care/model/doctor.dart';
import '../../components/card.dart';
import 'package:flutter/services.dart';
import 'package:toastification/toastification.dart';
// import '../../context/app.dart';

class Doctors extends StatefulWidget {
  @override
  State<Doctors> createState() => _DoctorsState();
}

class _DoctorsState extends State<Doctors> {
  List<Doctor> doctors = [];
  bool loaded = false;
  fetch() async {
    if (loaded) {
      return true;
    }
    final response = await http.get(
      Uri.parse('${Utils.url}/doctor'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    // print(response.body);
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      Map<String, dynamic> jsonData = json.decode(response.body);
      List<Doctor> d = [];
      jsonData.forEach((key, value) {
        if (value is List) {
          for (var element in value) {
            d.add(Doctor.fromJson(element));
          }
        }
      });
      // print(d);
      setState(() {
        doctors = d;
        loaded = true;
      });
    } else {
      throw Exception('Failed to load patients.');
    }
  }

  @override
  Widget build(BuildContext context) {
    // var appState = context.read()<MyAppState>();
    // var pair = appState.current;
    // print('object');
    fetch();
    // print('objects');
    List<Widget> widgets = [];
    for (var doctor in doctors) {
      // print('looping');
      widgets.add(
        Card(
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: [
              ListTile(
                leading: Icon(Icons.arrow_drop_down_circle),
                title: Text(doctor.name),
                subtitle: Text(
                  'Gender - ${doctor.gender}, ID - ${doctor.id}',
                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                ),
              ),
              // Padding(
              //   padding: const EdgeInsets.all(16.0),
              //   child: Text(
              //     'Greyhound divisively hello coldly wonderfully marginally far upon excluding.',
              //     style: TextStyle(color: Colors.black.withOpacity(0.6)),
              //   ),
              // ),
              ButtonBar(
                alignment: MainAxisAlignment.start,
                children: [
                  ElevatedButton(
                    // textColor: const Color(0xFF6200EE),
                    onPressed: () {
                      // Perform some action
                    },
                    child: const Text('On Duty'),
                  ),
                  ElevatedButton(
                    // textColor: const Color(0xFF6200EE),
                    onPressed: () async {
                      Clipboard.setData(ClipboardData(text: doctor.id ?? ''));
                      toastification.show(
                        context:
                            context, // optional if you use ToastificationWrapper
                        title: Text('Copied to clipboard'),
                        autoCloseDuration: const Duration(seconds: 5),
                        type: ToastificationType.info,
                        style: ToastificationStyle.flat,
                      );
                    },
                    child: const Text('Copy ID to Clipboard'),
                  ),
                  ElevatedButton(
                    // textColor: const Color(0xFF6200EE),
                    onPressed: () {
                      // Perform some action
                    },
                    child: const Text('View'),
                  ),
                ],
              ),
              // Image.asset('assets/card-sample-image.jpg'),
              // Image.asset('assets/card-sample-image-2.jpg'),
            ],
          ),
        ),
      );
    }
    return ListView(children: [
      Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            HeadingText(text: 'Doctors Page'),
            SubHeadingText(text: 'Activitiy Overview'),
            SizedBox(
              height: 25,
            ),
            Row(
              children: [
                Expanded(
                  child: BigCard(
                      widget: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          HeadingText(text: 'All Doctors'),
                          HeadingText(text: '28'),
                        ],
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SubHeadingText(text: 'Online'),
                          SubHeadingText(text: '5'),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(child: SizedBox()),
                          ElevatedButton(
                              onPressed: () {
                                // appState.updatePage(4);
                              },
                              child: Text('Add Doctor')),
                        ],
                      ),
                    ],
                  )),
                ),
                Expanded(
                  child: BigCard(
                      widget: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          HeadingText(text: 'Appointments'),
                          HeadingText(text: '12'),
                        ],
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SubHeadingText(text: 'Today'),
                          SubHeadingText(text: '2'),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(child: SizedBox()),
                          ElevatedButton(
                              onPressed: () {
                                // appState.updatePage(6);
                              },
                              child: Text('Schedule Appointment')),
                        ],
                      ),
                    ],
                  )),
                ),
              ],
            ),
            Column(
              children: loaded
                  ? widgets
                  : [
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: SubHeadingText(text: 'Loading Doctors...'),
                      )
                    ],
            )
          ],
        ),
      ),
    ]);
  }
}
