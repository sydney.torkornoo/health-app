import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rydemi_health_care/components/card.dart';
import 'package:rydemi_health_care/components/heading.dart';
import 'package:rydemi_health_care/components/sub_heading.dart';
import 'package:rydemi_health_care/helpers/util.dart';
import 'package:rydemi_health_care/model/patient.dart';
// import 'package:provider/provider.dart';
// import 'package:rydemi_health_care/components/heading.dart';
// import 'package:rydemi_health_care/components/input.dart';
// import 'package:rydemi_health_care/components/sub_heading.dart';
// import '../../components/card.dart';
import '../../context/app.dart';

class Patients extends StatefulWidget {
  @override
  State<Patients> createState() => patientsState();
}

// ignore: camel_case_types
class patientsState extends State<Patients> {
  List<Patient> patients = [];
  bool loaded = false;
  fetch() async {
    if (loaded) {
      return true;
    }
    final response = await http.get(
      Uri.parse('${Utils.url}/patient'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      Map<String, dynamic> jsonData = json.decode(response.body);
      List<Patient> p = [];
      jsonData.forEach((key, value) {
        if (value is List) {
          for (var element in value) {
            p.add(Patient.fromJson(element));
          }
        }
      });
      setState(() {
        patients = p;
        loaded = true;
      });
      // body.forEach((key, value) {
      //   print('$key, $value');
      // print(value.length);
      // });
      // final List ret = [];
      // print();
      // body['patients'].forEach((key, value) {});
      // body['patients'].map((e) {
      //   print(Patient.fromJson(e));
      //   ret.add(Patient.fromJson(e));
      // });
      // print(body);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to load patients.');
    }
  }

  checkPatient(String patientId, String status) async {
    final response = await http.patch(
      Uri.parse('${Utils.url}/patient/$patientId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'status': status,
      }),
    );

    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.

      setState(() {
        loaded = false;
      });
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to load patients.');
    }
  }

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    List<Widget> widgets = [];
    fetch();

    widgets.add(HeadingText(text: 'All Hospital Patients'));
    widgets.add(
      Row(
        children: [
          Expanded(
            // flex: 2,
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'All Patients'),
                    HeadingText(text: '330'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'Checked-in Patients'),
                    SubHeadingText(text: '25'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    ElevatedButton(
                        onPressed: () {
                          appState.updatePage(5);
                        },
                        child: Text('Add Patient')),
                    SizedBox(
                      width: 5,
                    ),
                    // ElevatedButton(onPressed: () {}, child: Text('View')),
                  ],
                ),
              ],
            )),
          ),
          Expanded(
            child: BigCard(
                widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    HeadingText(text: 'Quick Actions'),
                    // HeadingText(text: '28'),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SubHeadingText(text: 'Online'),
                    SubHeadingText(text: '5'),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: SizedBox()),
                    ElevatedButton(
                        onPressed: () {
                          appState.updatePage(4);
                        },
                        child: Text('Add Doctor')),
                  ],
                ),
              ],
            )),
          ),
        ],
      ),
    );

    for (var patient in patients) {
      widgets.add(SizedBox(
        child: Card(
          clipBehavior: Clip.antiAlias,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  radius: 50,
                  backgroundImage: AssetImage('images/bg.png'),
                ),
              ),
              ListTile(
                // leading: Icon(Icons.arrow_drop_down_circle),
                title: Text(patient.name),
                subtitle: Text(
                  'DOB - ${patient.dob}',
                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  'ID - ${patient.id}',
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  'Gender - ${patient.gender}, Status - ${patient.status.replaceAll(RegExp(r'-'), ' ').toUpperCase()}',
                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                ),
              ),
              ButtonBar(
                alignment: MainAxisAlignment.start,
                children: patient.status == 'checked-in'
                    ? [
                        ElevatedButton(
                          // textColor: const Color(0xFF6200EE),
                          onPressed: () {
                            appState.setPatient(patient);
                            appState.updatePage(8);
                            // Perform some action
                          },
                          child: const Text('Add Record'),
                        ),
                        ElevatedButton(
                          // textColor: const Color(0xFF6200EE),
                          onPressed: () {
                            // Perform some action
                            checkPatient(patient.id, 'checked-out');
                          },
                          child: const Text('Check-out Patient'),
                        ),
                        ElevatedButton(
                          // textColor: const Color(0xFF6200EE),
                          onPressed: () {
                            appState.setPatient(patient);
                            appState.updatePage(6);
                          },
                          child: const Text('Schedule Appointment'),
                        ),
                        ElevatedButton(
                          // textColor: const Color(0xFF6200EE),
                          onPressed: () {
                            appState.setPatient(patient);
                            appState.updatePage(7);
                            // Perform some action
                            // checkPatient(patient.id, 'checked-in');
                          },
                          child: const Text('View'),
                        ),
                      ]
                    : [
                        ElevatedButton(
                          // textColor: const Color(0xFF6200EE),
                          onPressed: () {
                            // Perform some action
                            checkPatient(patient.id, 'checked-in');
                          },
                          child: const Text('Check-in Patient'),
                        ),
                        ElevatedButton(
                          // textColor: const Color(0xFF6200EE),
                          onPressed: () {
                            // Perform some action
                            // checkPatient(patient.id, 'checked-out');
                            appState.setPatient(patient);
                            appState.updatePage(6);
                          },
                          child: const Text('Schedule Appointment'),
                        ),
                        ElevatedButton(
                          // textColor: const Color(0xFF6200EE),
                          onPressed: () {
                            // Perform some action
                            appState.setPatient(patient);
                            appState.updatePage(7);
                          },
                          child: const Text('View'),
                        ),
                      ],
              ),
              // Image.asset('assets/card-sample-image.jpg'),
              // Image.asset('assets/card-sample-image-2.jpg'),
            ],
          ),
        ),
      ));
    }

    return ListView(
      padding: const EdgeInsets.all(8),
      children: loaded
          ? widgets
          : [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: SubHeadingText(text: 'Loading Patients...'),
              )
            ],
    );
  }
}
