import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rydemi_health_care/components/heading.dart';
import 'package:rydemi_health_care/components/input.dart';
import 'package:rydemi_health_care/components/sub_heading.dart';
import '../context/app.dart';

class LoginPage extends StatefulWidget {
  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var email = '';
  var password = '';
  var wrongCredentials = false;
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    // var pair = appState.current;

    // IconData icon;
    // if (appState.favourites.contains(pair)) {
    //   icon = Icons.favorite;
    // } else {
    //   icon = Icons.favorite_border;
    // }

    return Center(
      child: Row(
        children: [
          // SizedBox(
          //   width: 20,
          // ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                HeadingText(text: 'Welcome!'),
                SubHeadingText(text: 'Please log into your account'),
                SizedBox(height: 10),
                SizedBox(
                  width: 250,
                  child: FormInput(
                      label: 'Enter Email',
                      f: (v) {
                        setState(() {
                          email = v;
                          wrongCredentials = false;
                        });
                      }),
                ),
                SizedBox(
                  width: 250,
                  child: FormInput(
                    label: 'Enter Password',
                    f: (v) {
                      setState(() {
                        password = v;
                        wrongCredentials = false;
                      });
                    },
                    obscureText: true,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Column(
                  children: wrongCredentials
                      ? [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            child: SizedBox(
                              child: Text(
                                'Login failed: Wrong Credentials.',
                                style: TextStyle(color: Colors.red),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 25,
                          ),
                        ]
                      : [SizedBox()],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(width: 10),
                      ElevatedButton(
                        onPressed: () {
                          if (email == 'admin@healthapp.com' &&
                              password == 'admin123') {
                            appState.login();
                          } else {
                            setState(() {
                              wrongCredentials = true;
                            });
                          }
                        },
                        child: SizedBox(
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Text('Login'),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Center(
              child: Image(
                  image: AssetImage("images/bg.png"), fit: BoxFit.contain),
            ),
          ),
        ],
      ),
    );
  }
}
