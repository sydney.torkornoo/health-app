import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rydemi_health_care/components/nav.dart';
import 'package:rydemi_health_care/pages/login.dart';
import 'package:rydemi_health_care/pages/staff/add_doctor.dart';
import 'package:rydemi_health_care/pages/staff/add_log.dart';
import 'package:rydemi_health_care/pages/staff/add_patient.dart';
import 'package:rydemi_health_care/pages/staff/appointments.dart';
import 'package:rydemi_health_care/pages/staff/dashboard.dart';
import 'package:rydemi_health_care/pages/staff/doctors.dart';
import 'package:rydemi_health_care/pages/staff/patient_record.dart';
import 'package:rydemi_health_care/pages/staff/patients.dart';
import 'package:rydemi_health_care/pages/staff/schedule_appointment.dart';
import 'package:toastification/toastification.dart';
import './context/app.dart';
// import 'package:firebase_core/firebase_core.dart';
// import 'firebase_options.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => MyAppState(),
      child: ToastificationWrapper(
        child: MaterialApp(
          title: 'RydeMi Health',
          theme: ThemeData(
            useMaterial3: true,
            fontFamily: 'Teachers',
            // primarySwatch: Colors.blue,
            colorScheme: ColorScheme.fromSeed(
              seedColor: Color.fromARGB(255, 183, 207, 210),
            ),
          ),
          home: MyHomePage(),
        ),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final hideNav = false;

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    var pageIndex = appState.pageIndex;

    Widget page;
    switch (pageIndex) {
      case 0:
        page = Dashboard();
      case 1:
        page = Patients();
      case 2:
        page = Doctors();
      case 3:
        page = Appointments();
      case 4:
        page = AddDoctor();
      case 5:
        page = AddPatient();
      case 6:
        page = ScheduleAppointment();
      case 7:
        page = PatientRecord();
      case 8:
        page = AddLog();
      default:
        page = Text('no widget for $pageIndex');
    }
    return Scaffold(
      // return Scaffold(
      body: Row(
        children: [
          appState.loggedIn ? SafeArea(child: MainNavigation()) : Container(),
          Expanded(
            child: Container(
              color: Theme.of(context).colorScheme.primaryContainer,
              child: appState.loggedIn ? page : LoginPage(),
            ),
          ),
        ],
      ),
      // );
    );
  }
}
